let collection = [];

// Write the queue functions below.
// To test your skills, try to use pure javascript coding and avoid using pop, push, slice, splice, etc.

function print() {
	// output all the elements of the queue
    return collection;
}

function enqueue(element) {
	// add element to rear of queue

    for(var i=0; i<collection.length; i++){
        if(collection[i] === collection.length-1)
        element++;
    }
    return element
}

function dequeue() {
	// remove element at front of queue
    var front = 0
    for (i=0; i < collection.length; i--)
        if(collection[i] === collection[0]){
            front--
        }
        return front
}

function front() {
	// show element at the front
    return collection[0];
}

function size() {
    // show total number of elements
    return collection.length;
}

function isEmpty() {
    // outputs Boolean value describing whether queue is empty or not
    if (collection = null){
        return true;
    }else{
        return false;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
